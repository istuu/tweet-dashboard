<!DOCTYPE html>
<html lang="en" data-footer="true">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>Tweet Dashboard App</title>
        <meta
            name="description"
            content="A table enhancing plug-in for the jQuery Javascript library, adding sorting, paging and filtering abilities to plain HTML tables with minimal effort."
            />
        <!-- Favicon Tags Start -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="img/favicon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="img/favicon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="img/favicon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="img/favicon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="img/favicon/mstile-310x310.png" />
        <!-- Favicon Tags End -->
        <!-- Font Tags Start -->
        <link rel="preconnect" href="{{ asset('assets') }}/https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('assets') }}/font/CS-Interface/style.css" />
        <!-- Font Tags End -->
        <!-- Vendor Styles Start -->
        <link rel="stylesheet" href="{{ asset('assets') }}/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="{{ asset('assets') }}/css/vendor/OverlayScrollbars.min.css" />
        <link rel="stylesheet" href="{{ asset('assets') }}/css/vendor/datatables.min.css" />
        <!-- Vendor Styles End -->
        <!-- Template Base Styles Start -->
        <link rel="stylesheet" href="{{ asset('assets') }}/css/styles.css" />
        <!-- Template Base Styles End -->
        <link rel="stylesheet" href="{{ asset('assets') }}/css/main.css" />
        <script src="{{ asset('assets') }}/js/base/loader.js"></script>
    </head>
    <body>
        <div id="root">
            <div id="nav" class="nav-container d-flex">
                <div class="nav-content d-flex">
                    <!-- Logo Start -->
                    <div class="logo position-relative">
                        <a href="{{ url('/') }}">
                            <!-- Logo can be added directly -->
                            <!-- <img src="{{ asset('assets') }}/img/logo/logo-white.svg" alt="logo" /> -->
                            <!-- Or added via css to provide different ones for different color themes -->
                            <div class="img"></div>
                        </a>
                    </div>
                    <!-- Logo End -->
                    <!-- Icons Menu Start -->
                    <ul class="list-unstyled list-inline text-center menu-icons">
                    </ul>
                    <!-- Icons Menu End -->
                    <!-- Menu Start -->
                    <div class="menu-container flex-grow-1">
                        <ul id="menu" class="menu">
                        </ul>
                    </div>
                    <!-- Menu End -->
                    <!-- Mobile Buttons Start -->
                    <div class="mobile-buttons-container">
                        <!-- Scrollspy Mobile Button Start -->
                        <a href="{{ asset('assets') }}/#" id="scrollSpyButton" class="spy-button" data-bs-toggle="dropdown">
                        <i data-cs-icon="menu-dropdown"></i>
                        </a>
                        <!-- Scrollspy Mobile Button End -->
                        <!-- Scrollspy Mobile Dropdown Start -->
                        <div class="dropdown-menu dropdown-menu-end" id="scrollSpyDropdown"></div>
                        <!-- Scrollspy Mobile Dropdown End -->
                        <!-- Menu Button Start -->
                        <a href="{{ asset('assets') }}/#" id="mobileMenuButton" class="menu-button">
                        <i data-cs-icon="menu"></i>
                        </a>
                        <!-- Menu Button End -->
                    </div>
                    <!-- Mobile Buttons End -->
                </div>
                <div class="nav-shadow"></div>
            </div>
            <main>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <!-- Title and Top Buttons Start -->
                            <div class="page-title-container">
                                <div class="row">
                                    <!-- Title Start -->
                                    <div class="col-12 col-md-7">
                                        <h1 class="mb-0 pb-0 display-4" id="title">Tweet Dashboard Data</h1>
                                    </div>
                                    <!-- Title End -->
                                    <!-- Top Buttons Start -->
                                    <div class="col-12 col-md-5 d-flex align-items-start justify-content-end">
                                        <!-- Add New Button Start -->
                                        <button type="button" data-bs-toggle="modal" data-bs-target="#modal-import" class="btn btn-outline-primary btn-icon btn-icon-start w-100 w-md-auto">
                                        <i data-cs-icon="plus"></i>
                                        <span>Import Data</span>
                                        </button>
                                        <!-- Add New Button End -->
                                        <!-- Check Button Start -->
                                        <div class="btn-group ms-1 check-all-container">
                                            <div class="btn btn-outline-primary btn-custom-control p-0 ps-3 pe-2" id="datatableCheckAllButton">
                                                <span class="form-check float-end">
                                                <input type="checkbox" class="form-check-input" id="datatableCheckAll" />
                                                </span>
                                            </div>
                                            <button
                                                type="button"
                                                class="btn btn-outline-primary dropdown-toggle dropdown-toggle-split"
                                                data-bs-offset="0,3"
                                                data-bs-toggle="dropdown"
                                                aria-haspopup="true"
                                                aria-expanded="false"
                                                data-submenu
                                                ></button>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <button class="dropdown-item disabled delete-datatable" type="button">Delete</button>
                                            </div>
                                        </div>
                                        <!-- Check Button End -->
                                    </div>
                                    <!-- Top Buttons End -->
                                </div>
                            </div>
                            <!-- Title and Top Buttons End -->
                            <!-- Content Start -->
                            <div class="data-table-rows slim">
                                <!-- Controls Start -->
                                <div class="row">
                                    <!-- Search Start -->
                                    <div class="col-sm-12 col-md-5 col-lg-3 col-xxl-2 mb-1">
                                        <div class="d-inline-block float-md-start me-1 mb-1 search-input-container w-100 shadow bg-foreground">
                                            <input class="form-control datatable-search" placeholder="Search" data-datatable="#datatableRowsServerSide" />
                                            <span class="search-magnifier-icon">
                                            <i data-cs-icon="search"></i>
                                            </span>
                                            <span class="search-delete-icon d-none">
                                            <i data-cs-icon="close"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- Search End -->
                                    <div class="col-sm-12 col-md-7 col-lg-9 col-xxl-10 text-end mb-1">
                                        <div class="d-inline-block me-0 me-sm-3 float-start float-md-none">
                                            <!-- Edit Button Start -->
                                            <button
                                                class="btn btn-icon btn-icon-only btn-foreground-alternate shadow edit-datatable disabled"
                                                data-bs-delay="0"
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top"
                                                title="Edit"
                                                type="button"
                                                >
                                            <i data-cs-icon="edit"></i>
                                            </button>
                                            <!-- Edit Button End -->
                                            <!-- Delete Button Start -->
                                            <button
                                                class="btn btn-icon btn-icon-only btn-foreground-alternate shadow disabled delete-datatable"
                                                data-bs-delay="0"
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top"
                                                title="Delete"
                                                type="button"
                                                >
                                            <i data-cs-icon="bin"></i>
                                            </button>
                                            <!-- Delete Button End -->
                                        </div>
                                        <div class="d-inline-block">
                                            <!-- Print Button Start -->
                                            <button
                                                class="btn btn-icon btn-icon-only btn-foreground-alternate shadow datatable-print"
                                                data-bs-delay="0"
                                                data-datatable="#datatableRowsServerSide"
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top"
                                                title="Print"
                                                type="button"
                                                >
                                            <i data-cs-icon="print"></i>
                                            </button>
                                            <!-- Print Button End -->
                                            <!-- Export Dropdown Start -->
                                            <div class="d-inline-block datatable-export" data-datatable="#datatableRowsServerSide">
                                                <button class="btn p-0" data-bs-toggle="dropdown" type="button" data-bs-offset="0,3">
                                                <span
                                                    class="btn btn-icon btn-icon-only btn-foreground-alternate shadow dropdown"
                                                    data-bs-delay="0"
                                                    data-bs-placement="top"
                                                    data-bs-toggle="tooltip"
                                                    title="Export"
                                                    >
                                                <i data-cs-icon="download"></i>
                                                </span>
                                                </button>
                                                <div class="dropdown-menu shadow dropdown-menu-end">
                                                    <button class="dropdown-item export-copy" type="button">Copy</button>
                                                    <button class="dropdown-item export-excel" type="button">Excel</button>
                                                    <button class="dropdown-item export-cvs" type="button">Cvs</button>
                                                </div>
                                            </div>
                                            <!-- Export Dropdown End -->
                                            <!-- Length Start -->
                                            <div class="dropdown-as-select d-inline-block datatable-length" data-datatable="#datatableRowsServerSide" data-childSelector="span">
                                                <button class="btn p-0 shadow" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-bs-offset="0,3">
                                                <span
                                                    class="btn btn-foreground-alternate dropdown-toggle"
                                                    data-bs-toggle="tooltip"
                                                    data-bs-placement="top"
                                                    data-bs-delay="0"
                                                    title="Item Count"
                                                    >
                                                10 Items
                                                </span>
                                                </button>
                                                <div class="dropdown-menu shadow dropdown-menu-end">
                                                    <a class="dropdown-item" href="{{ url('/') }}#">5 Items</a>
                                                    <a class="dropdown-item active" href="{{ url('/') }}#">10 Items</a>
                                                    <a class="dropdown-item" href="{{ url('/') }}#">20 Items</a>
                                                </div>
                                            </div>
                                            <!-- Length End -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Controls End -->
                                <!-- Table Start -->
                                <div class="data-table-responsive-wrapper">
                                    <br>
                                    @if(Session::has('success'))
                                        <div class="alert alert-success" role="alert">Success Importing Data!</div>
                                    @endif
                                    @if ($errors->any())
                                        <div class="alert alert-danger" role="alert">Whoops! Failed to Import Data.</div>
                                    @endif

                                    <table id="datatableRowsServerSide" class="data-table hover">
                                        <thead>
                                            <tr>
                                                <th class="text-muted text-small text-uppercase">Twitter ID</th>
                                                <th class="text-muted text-small text-uppercase">Twitter Type</th>
                                                <th class="text-muted text-small text-uppercase">Label</th>
                                                <th class="text-muted text-small text-uppercase">Description</th>
                                                <th class="text-muted text-small text-uppercase">Created At</th>
                                                <th class="empty">&nbsp;</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <!-- Table End -->
                            </div>
                            <!-- Content End -->
                            <!-- Add Edit Modal Start -->
                            <div class="modal fade" id="modal-import" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ url('import') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalTitle">Import Data</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="mb-3">
                                                    <label class="form-label">Name</label>
                                                    <input name="file" type="file" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-outline-primary" data-bs-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Import</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Edit Modal End -->
                        </div>
                    </div>
                </div>
            </main>
            <!-- Layout Footer Start -->
            <footer>
                <div class="footer-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <!-- <p class="mb-0 text-muted text-medium"> 2021</p> -->
                            </div>
                            <div class="col-sm-6 d-none d-sm-block">
                                <!-- <ul class="breadcrumb pt-0 pe-0 mb-0 float-end">
                                    <li class="breadcrumb-item mb-0 text-medium"><a href="{{ asset('assets') }}/#" class="btn-link">Review</a></li>
                                    <li class="breadcrumb-item mb-0 text-medium"><a href="{{ asset('assets') }}/#" class="btn-link">Purchase</a></li>
                                    <li class="breadcrumb-item mb-0 text-medium"><a href="{{ asset('assets') }}/#" class="btn-link">Docs</a></li>
                                    </ul> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- Layout Footer End -->
        </div>
        <!-- Vendor Scripts Start -->
        <script src="{{ asset('assets') }}/js/vendor/jquery-3.5.1.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/OverlayScrollbars.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/autoComplete.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/clamp.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/bootstrap-submenu.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/datatables.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/mousetrap.min.js"></script>
        <!-- Vendor Scripts End -->
        <!-- Template Base Scripts Start -->
        <script src="{{ asset('assets') }}/font/CS-Line/csicons.min.js"></script>
        <script src="{{ asset('assets') }}/js/base/helpers.js"></script>
        <script src="{{ asset('assets') }}/js/base/globals.js"></script>
        <script src="{{ asset('assets') }}/js/base/nav.js"></script>
        <script src="{{ asset('assets') }}/js/base/search.js"></script>
        <script src="{{ asset('assets') }}/js/base/settings.js"></script>
        <script src="{{ asset('assets') }}/js/base/init.js"></script>
        <!-- Template Base Scripts End -->
        <!-- Page Specific Scripts Start -->
        <script src="{{ asset('assets') }}/js/cs/datatable.extend.js"></script>
        <script src="{{ asset('assets') }}/js/plugins/datatable.serverside.js"></script>
        <script src="{{ asset('assets') }}/js/common.js"></script>
        <script src="{{ asset('assets') }}/js/scripts.js"></script>
        <!-- Page Specific Scripts End -->
    </body>
</html>
