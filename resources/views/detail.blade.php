<!DOCTYPE html>
<html lang="en" data-footer="true">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>Tweet Dashboard App</title>
        <meta
            name="description"
            content="A table enhancing plug-in for the jQuery Javascript library, adding sorting, paging and filtering abilities to plain HTML tables with minimal effort."
            />
        <!-- Favicon Tags Start -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('assets') }}/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/favicon/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="img/favicon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="img/favicon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="img/favicon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="img/favicon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="img/favicon/mstile-310x310.png" />
        <!-- Favicon Tags End -->
        <!-- Font Tags Start -->
        <link rel="preconnect" href="{{ asset('assets') }}/https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('assets') }}/font/CS-Interface/style.css" />
        <!-- Font Tags End -->
        <!-- Vendor Styles Start -->
        <link rel="stylesheet" href="{{ asset('assets') }}/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="{{ asset('assets') }}/css/vendor/OverlayScrollbars.min.css" />
        <link rel="stylesheet" href="{{ asset('assets') }}/css/vendor/datatables.min.css" />
        <!-- Vendor Styles End -->
        <!-- Template Base Styles Start -->
        <link rel="stylesheet" href="{{ asset('assets') }}/css/styles.css" />
        <!-- Template Base Styles End -->
        <link rel="stylesheet" href="{{ asset('assets') }}/css/main.css" />
        <script src="{{ asset('assets') }}/js/base/loader.js"></script>
    </head>
    <body>
        <div id="root">
            <div id="nav" class="nav-container d-flex">
                <div class="nav-content d-flex">
                    <!-- Logo Start -->
                    <div class="logo position-relative">
                        <a href="{{ url('/') }}">
                            <!-- Logo can be added directly -->
                            <!-- <img src="{{ asset('assets') }}/img/logo/logo-white.svg" alt="logo" /> -->
                            <!-- Or added via css to provide different ones for different color themes -->
                            <div class="img"></div>
                        </a>
                    </div>
                    <!-- Logo End -->
                    <!-- Icons Menu Start -->
                    <ul class="list-unstyled list-inline text-center menu-icons">
                    </ul>
                    <!-- Icons Menu End -->
                    <!-- Menu Start -->
                    <div class="menu-container flex-grow-1">
                        <ul id="menu" class="menu">
                        </ul>
                    </div>
                    <!-- Menu End -->
                    <!-- Mobile Buttons Start -->
                    <div class="mobile-buttons-container">
                        <!-- Scrollspy Mobile Button Start -->
                        <a href="{{ asset('assets') }}/#" id="scrollSpyButton" class="spy-button" data-bs-toggle="dropdown">
                        <i data-cs-icon="menu-dropdown"></i>
                        </a>
                        <!-- Scrollspy Mobile Button End -->
                        <!-- Scrollspy Mobile Dropdown Start -->
                        <div class="dropdown-menu dropdown-menu-end" id="scrollSpyDropdown"></div>
                        <!-- Scrollspy Mobile Dropdown End -->
                        <!-- Menu Button Start -->
                        <a href="{{ asset('assets') }}/#" id="mobileMenuButton" class="menu-button">
                        <i data-cs-icon="menu"></i>
                        </a>
                        <!-- Menu Button End -->
                    </div>
                    <!-- Mobile Buttons End -->
                </div>
                <div class="nav-shadow"></div>
            </div>
            <main>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <!-- Title and Top Buttons Start -->
                            <div class="page-title-container">
                                <div class="row">
                                    <!-- Title Start -->
                                    <div class="col-12 col-md-7">
                                        <h1 class="mb-0 pb-0 display-4" id="title">Tweet Data Detail</h1>
                                    </div>
                                </div>
                            </div>
                            <!-- Title and Top Buttons End -->
                            <!-- Content Start -->
                            <div class="data-table-rows slim">
                                <!-- Table Start -->
                                <div class="">
                                    <table class="table table-striped table-hover">
                                        @foreach($model as $key => $value)
                                            @if($value !== null)
                                                <tr>
                                                    <td width="30%">{{ ucwords(str_replace("_", " ", $key)) }}</td>
                                                    <td width="1%">:</td>
                                                    <td>{{ $value }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </table>
                                </div>
                                <a class="btn btn-primary" href="{{ url('/') }}"> Back to Dashboard </a>
                                <!-- Table End -->
                            </div>
                            <!-- Content End -->
                            <!-- Add Edit Modal Start -->
                            <div class="modal fade" id="modal-import" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ url('import') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalTitle">Import Data</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="mb-3">
                                                    <label class="form-label">Name</label>
                                                    <input name="file" type="file" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-outline-primary" data-bs-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Import</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Edit Modal End -->
                        </div>
                    </div>
                </div>
            </main>
            <!-- Layout Footer Start -->
            <footer>
                <div class="footer-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <!-- <p class="mb-0 text-muted text-medium"> 2021</p> -->
                            </div>
                            <div class="col-sm-6 d-none d-sm-block">
                                <!-- <ul class="breadcrumb pt-0 pe-0 mb-0 float-end">
                                    <li class="breadcrumb-item mb-0 text-medium"><a href="{{ asset('assets') }}/#" class="btn-link">Review</a></li>
                                    <li class="breadcrumb-item mb-0 text-medium"><a href="{{ asset('assets') }}/#" class="btn-link">Purchase</a></li>
                                    <li class="breadcrumb-item mb-0 text-medium"><a href="{{ asset('assets') }}/#" class="btn-link">Docs</a></li>
                                    </ul> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- Layout Footer End -->
        </div>
        <!-- Vendor Scripts Start -->
        <script src="{{ asset('assets') }}/js/vendor/jquery-3.5.1.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/OverlayScrollbars.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/autoComplete.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/clamp.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/bootstrap-submenu.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/datatables.min.js"></script>
        <script src="{{ asset('assets') }}/js/vendor/mousetrap.min.js"></script>
        <!-- Vendor Scripts End -->
        <!-- Template Base Scripts Start -->
        <script src="{{ asset('assets') }}/font/CS-Line/csicons.min.js"></script>
        <script src="{{ asset('assets') }}/js/base/helpers.js"></script>
        <script src="{{ asset('assets') }}/js/base/globals.js"></script>
        <script src="{{ asset('assets') }}/js/base/nav.js"></script>
        <script src="{{ asset('assets') }}/js/base/search.js"></script>
        <script src="{{ asset('assets') }}/js/base/settings.js"></script>
        <script src="{{ asset('assets') }}/js/base/init.js"></script>
        <!-- Template Base Scripts End -->
        <!-- Page Specific Scripts Start -->
        <script src="{{ asset('assets') }}/js/cs/datatable.extend.js"></script>
        <script src="{{ asset('assets') }}/js/plugins/datatable.serverside.js"></script>
        <script src="{{ asset('assets') }}/js/common.js"></script>
        <script src="{{ asset('assets') }}/js/scripts.js"></script>
        <!-- Page Specific Scripts End -->
    </body>
</html>
