<?php

namespace App\Imports;

use App\Models\Tweet;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Str;

class TweetImport implements ToModel
{
    private $arrMonth = [
        'Jan' => 1,
        'Feb' => 2,
        'Mar' => 3,
        'Apr' => 4,
        'May' => 5,
        'Jun' => 6,
        'Jul' => 6,
        'Aug' => 8,
        'Sep' => 9,
        'Oct' => 10,
        'Nov' => 11,
        'Dec' => 12,
    ];

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] !== 'Id' && $row[0] !== null) {
            $tweetCreatedAt = null;
            if ($row[10] !== null) {
                $col = Str::of($row[10])->explode(' ');
                $formatTime = $col[5] . '-' . $this->arrMonth[$col[1]] . '-' . $col[2] . ' ' . $col[3];
                $tweetCreatedAt = date('Y-m-d H:i:s', strtotime($formatTime));
            }
            return new Tweet([
                'twitter_id' => $row[0],
                'twitter_type' => $row[3],
                'label' => $row[1],
                'timeset' => $row[2],
                'latitute' => $row[4],
                'longitute' => $row[5],
                'place_country' => $row[6],
                'place_type' => $row[7],
                'place_fullname' => $row[8],
                'place_name' => $row[9],
                'tweet_created_at' => $tweetCreatedAt,
                'language' => $row[11],
                'possibility_sensitive' => $row[12] === 'true' ? 1:0,
                'quoted_status_permalink' => $row[13],
                'description' => $row[14],
                'email' => $row[15],
                'profil_image' => $row[16],
                'friends_count' => $row[17],
                'followers_count' => $row[18],
                'realname' => $row[19],
                'location' => $row[20],
                'emoji_alias' => $row[21],
                'emoji_html_decimal' => $row[22],
                'emoji_utf8' => $row[23],
            ]);
        }

    }
}
