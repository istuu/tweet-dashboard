<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tweet as Model;
use App\Imports\TweetImport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;

class TweetController extends Controller
{
    /**
     * Show the index page of tweet dashboard
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Get data tweets from database to datatable
     *
     * @return json data
     */
    public function getData()
    {
        $model = Model::query();

        return DataTables::eloquent($model)
                ->make();
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import(Request $request)
    {
        $validated = $request->validate([
            'file' => 'required|file|max:500000',
        ]);

        if ($validated) {
            try{
                Excel::import(new TweetImport,request()->file('file'));

                return redirect()->back()->with('success', 'Success Import Data');
            }catch(\Exception $e) {
                dd($e);
            }
        } else {
            return redirect()->back()->withErrors(['msg' => 'File is not valid or larger than 5MB']);
        }

    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function detail(Request $request, $id)
    {
        $data = Model::findOrFail($id);
        $model = $data->toArray();

        return view('detail', compact('model'));

    }
}
