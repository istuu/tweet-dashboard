<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TweetController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TweetController::class, 'index']);
Route::get('/get-data', [TweetController::class, 'getData']);
Route::post('/import', [TweetController::class, 'import']);
Route::get('/detail/{id}', [TweetController::class, 'detail']);
