<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->id();
            $table->string('twitter_id');
            $table->text('twitter_type');
            $table->text('label');
            $table->string('timeset');
            $table->string('latitute')->nullable();
            $table->string('longitute')->nullable();
            $table->string('place_country')->nullable();
            $table->string('place_type')->nullable();
            $table->string('place_fullname')->nullable();
            $table->string('place_name')->nullable();
            $table->datetime('tweet_created_at')->nullable();
            $table->string('language')->nullable();
            $table->boolean('possibility_sensitive')->default(false);
            $table->string('quoted_status_permalink')->nullable();
            $table->text('description')->nullable();
            $table->string('email')->nullable();
            $table->string('profil_image')->nullable();
            $table->string('friends_count')->nullable();
            $table->string('followers_count')->nullable();
            $table->string('realname')->nullable();
            $table->string('location')->nullable();
            $table->string('emoji_alias')->nullable();
            $table->string('emoji_html_decimal')->nullable();
            $table->string('emoji_utf8')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweets');
    }
}
